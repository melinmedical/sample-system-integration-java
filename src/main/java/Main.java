import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.patientsky.integrationcore.authentication.AuthenticateResponse;
import com.patientsky.integrationcore.authentication.HttpAuthentication;

public class Main {
    public static void main (String args[]) throws JsonProcessingException {
        String environmentUrl = "https://development-partnergateway.gel.camp";
        String username = "mg4ljXR8KDqlYMC";
        String password = "_jIAp5ZK*hJ080g";
        //String tenantId = "442ba2fa-ebaa-4136-a54b-77fe0a959b95";

        AuthenticateResponse response = authenticate(environmentUrl,username,password);

        System.out.println(new ObjectMapper().writeValueAsString(response));
    }

    public static AuthenticateResponse authenticate(String environmentUrl, String username, String password) {
        return HttpAuthentication.authenticate(environmentUrl, username, password);
    }

}
